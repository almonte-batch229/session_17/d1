// console.log("Hello World!")

// [SECTION] Functions
// Functions in JS are lines or blocks of code that will tell our device or application to perform a particular task.
// Functions are mostly created to create complicated tasks to run several lines of code succession.
/* 	Syntax:
		function functionName() {
			Code Block
			let number = 10;
			console.log(number)
		}
		
		console.log(number) -> error -> local scope variable */

function printGrade() {
	// console.log("My name is Hillary")
	let grade1 = 90;
	let grade2 = 95;
	let ave = (grade1 + grade2) / 2;

	console.log(ave)
}

// Function Call or Invocation
printGrade();

// [SECTION] Function Invocation will execute the block of codes inside the function being called

// this will call the function named printGrade()
printGrade();

// sampleInvocation() -> undeclared functions cannot be invoked.

// [SECTION] Function Declaration vs Expression

// Function Declaration
function declaredFunction() {
	console.log("Hello World from declaredFunction")
}

declaredFunction();

// Function Expression -> a function can also be stored in a variable.

// variableFunction(); -> error -> function expressions being stored in a let or const, cannot be hoisted

// Anonymous Function -> a function without a name
let variableFunction = function() {
	console.log("Hello Again!")
}

variableFunction();

let functionExpression = function functionName() {
	console.log("Hello from the other side")
}

// This is the right invocation in function expreesion
functionExpression();

// functionName(); -> this will return an error because we cannot use the functionName when we stroe it in a variable

// You can re-assign declared functions and function expression to a new anonymous function
declaredFunction = function() {
	console.log("updated declaredFunction")
}

declaredFunction()


functionExpression = function() {
	console.log("updated functionExpression")
}

functionExpression()

// However, we cannot reassign a function expression initialized with const

const constantFunction = function() {
	console.log("Initialized with a const")
}

constantFunction()

/*	constantFunction = function() {
		console.log("Will try to reassign")
	}

constantFunction(); */

// [SECTION] Function Scope
/*	1. Local/Block Scope
	2. GLobal Scope
	3. Function Scope */

{
	// this is a local variable and its value is only accessible inside the curly braces.
	let localVariable = "Armando Perez"

	console.log(localVariable)
}

// this is a global variable and its value is accessible anywhere in the codebase.
let globalVariable = "Mr. WorldWide"

console.log(globalVariable)
// console.log(localVariable) -> will return an error because we can only use it inside the curly braces

function showNames() {
	// this is a function scope variables
	var functionVar = "Joe"
	const functionConst = "John"
	let functionLet = "Jane"

	console.log(functionVar)
	console.log(functionConst)
	console.log(functionLet)
}

showNames()

// console.log(functionVar) -> will return an error because we can only use it inside the function named showNames()
// console.log(functionConst) -> will return an error because we can only use it inside the function named showNames()
// console.log(functionLet) -> will return an error because we can only use it inside the function named showNames()

// Nested Function

function myNewFunction() {
	let name = "Jane"

	function nestedFunction() {
		let nestedName = "John"

		console.log(nestedName)
		console.log(name)
	}

	nestedFunction()

	// console.log(nestedName) -> will return an error because we can only use it inside the child function
}

myNewFunction()

// nestedFunction() -> will return an error because we can only use it inside the function named myNewFunction()

// Function and Global Scope Variables
let globalName = "Alexander"

function myNewFunction2() {
	let nameInside = "Renz"

	console.log(globalName)
}

myNewFunction2()

// console.log(nameInside) will return an error because we can only use it inside the function named myNewFunction2()

// [SECTION] Using Alert
// alert() allows us to show a small window at the top of our browser
// show only alert() for short dialog message
// do not overuse alert() because program/js has to wait for it to be dismissed before continuing

// alert("Hello World!")

function showSampleAlert() {
	alert("Hello User!")
}

showSampleAlert()

console.log("I will only log in the console when the alert is dismissed")

// [SECTION] Using Prompt
// prompt() allows us to show a small window and gather user input

let samplePrompt = prompt("Enter your name:")

console.log("Hello, " + samplePrompt + "!")
console.log(typeof samplePrompt)

let sampleNullPrompt = prompt("Do not enter anything")

console.log(sampleNullPrompt) // returns an emoty string

function printWelcomeMessage() {
	let firstName = prompt("Please enter your first name")
	let lastName = prompt("Please enter your last name")

	console.log("Hello, " + firstName + " " + lastName)
	console.log("Welcome to my page!")
}

printWelcomeMessage()

// [SECTION] Function Naming Convention
// Function names should be definitive of the task it will perform. It usully contains verb.

function getCourses() {
	let courses = ["Science 101", "Math 101", "English 101"]

	console.log(courses)
}

getCourses()

// Avoid generic names to avoid confusion within your code

function get() {
	let name = "Jamie"

	console.log(name)
}

get()